from django import forms
from .models import Shop

class ShopForm(forms.ModelForm):
    DAYS_OF_WEEK = [
        ('monday', 'Monday'),
        ('tuesday', 'Tuesday'),
        ('wednesday', 'Wednesday'),
        ('thursday', 'Thursday'),
        ('friday', 'Friday'),
        ('saturday', 'Saturday'),
        ('sunday', 'Sunday'),
    ]

    for day in DAYS_OF_WEEK:
        locals()[f"{day[0]}_open"] = forms.TimeField(required=False, widget=forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}))
        locals()[f"{day[0]}_close"] = forms.TimeField(required=False, widget=forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}))
    
    class Meta:
        model = Shop
        exclude = ['opening_hours']  # Exclude the original ArrayField

        widgets = {
            'company': forms.TextInput(attrs={'class': 'form-control'}),
            'address': forms.TextInput(attrs={'class': 'form-control'}),
            'type': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'address_latitude': forms.NumberInput(attrs={'class': 'form-control'}),
            'address_longitude': forms.NumberInput(attrs={'class': 'form-control'}),
        }
        labels = {
            'company': 'Nazwa firmy',
            'address': 'Adres',
            'type': 'Typ sklepu',
            'description': 'Opis',
            'address_latitude': 'Współrzędne adresu (Szerokość geograficzna)',
            'address_longitude': 'Współrzędne adresu (Długość geograficzna)',
        }

    def clean(self):
        cleaned_data = super().clean()
        opening_hours = []
        for day, _ in self.DAYS_OF_WEEK:
            open_time = cleaned_data.get(f"{day}_open")
            close_time = cleaned_data.get(f"{day}_close")
            if open_time and not close_time:
                self.add_error(f"{day}_close", "Closing time required if opening time is provided.")
            elif close_time and not open_time:
                self.add_error(f"{day}_open", "Opening time required if closing time is provided.")
            elif open_time and close_time:
                if open_time >= close_time:
                    self.add_error(f"{day}_open", "Opening time must be before closing time.")
                opening_hours.append(f"{open_time.strftime('%H:%M')} - {close_time.strftime('%H:%M')}")
            else:
                opening_hours.append('')

        cleaned_data['opening_hours'] = opening_hours
        return cleaned_data


    def save(self, commit=True):
        instance = super().save(commit=False)
        print("1")
        instance.opening_hours = self.cleaned_data['opening_hours']
        print("2")
        if commit:
            print("3")
            instance.save()
        print("4")
        return instance

from django import forms
from .models import Shop

class EditShopForm(forms.ModelForm):
    DAYS_OF_WEEK = [
        ('monday', 'Monday'),
        ('tuesday', 'Tuesday'),
        ('wednesday', 'Wednesday'),
        ('thursday', 'Thursday'),
        ('friday', 'Friday'),
        ('saturday', 'Saturday'),
        ('sunday', 'Sunday'),
    ]

    for day in DAYS_OF_WEEK:
        locals()[f"{day[0]}_open"] = forms.TimeField(required=False, widget=forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}))
        locals()[f"{day[0]}_close"] = forms.TimeField(required=False, widget=forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}))

    class Meta:
        model = Shop
        exclude = ['owner', 'opening_hours']  # Exclude fields that are directly managed

    def __init__(self, *args, **kwargs):
        super(EditShopForm, self).__init__(*args, **kwargs)
        if self.instance and self.instance.opening_hours:
            for i, day in enumerate(self.DAYS_OF_WEEK):
                day_open, day_close = self.instance.opening_hours[i].split('-')
                self.fields[f'{day[0]}_open'].initial = day_open.strip()
                self.fields[f'{day[0]}_close'].initial = day_close.strip()

    def clean(self):
        cleaned_data = super().clean()
        opening_hours = []
        for day, _ in self.DAYS_OF_WEEK:
            open_time = cleaned_data.get(f"{day}_open")
            close_time = cleaned_data.get(f"{day}_close")
            if open_time and not close_time:
                self.add_error(f"{day}_close", "Closing time required if opening time is provided.")
            elif close_time and not open_time:
                self.add_error(f"{day}_open", "Opening time required if closing time is provided.")
            elif open_time and close_time:
                if open_time >= close_time:
                    self.add_error(f"{day}_open", "Opening time must be before closing time.")
                opening_hours.append(f"{open_time.strftime('%H:%M')} - {close_time.strftime('%H:%M')}")
            else:
                opening_hours.append('')

        cleaned_data['opening_hours'] = opening_hours
        return cleaned_data

    def save(self, commit=True):
        instance = super().save(commit=False)
        instance.opening_hours = self.cleaned_data['opening_hours']
        if commit:
            instance.save()
        return instance

