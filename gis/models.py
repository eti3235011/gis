from django.db import models
from django.contrib.postgres.fields import ArrayField
from .constants import HOUR_TEMPLATE
from django.contrib.auth.models import User
# Create your models here.


class Shop(models.Model):
    company = models.CharField(verbose_name="Nazwa firmy", max_length=128)
    address_latitude = models.DecimalField(verbose_name="Współrzędne adresu (Szerokość geograficzna)", max_digits=16,
                                           decimal_places=14)
    address_longitude = models.DecimalField(verbose_name="Współrzędne adresu (Długość geograficzna)", max_digits=16,
                                            decimal_places=14)
    address = models.CharField(verbose_name="Adres", max_length=128)
    opening_hours = ArrayField(models.CharField(max_length=128), null=True, blank=True, default=HOUR_TEMPLATE)

    type = models.CharField(verbose_name="Typ sklepu", max_length=128)
    description = models.CharField(verbose_name="Opis", max_length=512)
    owner = models.ForeignKey(User, verbose_name="Właściciel sklepu", on_delete=models.CASCADE, related_name="shops")
