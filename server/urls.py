"""
URL configuration for server project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from gis.views import ShopListView, MapView, map_view, LocationUpdateView, location_detail, marker_image, create_shop, UserShopsListView, ShopEditView
from server import settings
from django.conf.urls.static import static
from .admin import admin_site

urlpatterns = [
    path('login/', admin_site.urls),
    path('api/gis/shop/list/', ShopListView.as_view(), name='shop-list'),
    path('map/', MapView.as_view(), name='map'),
    # path('map/', map_view, name='map'),
    path('api/location/<int:pk>/', LocationUpdateView.as_view(), name='location-update'),
    path('map/static/images/marker.png', marker_image, name='marker_image'),
    path('new-shop/', create_shop, name='new_shop'),
    path('shop-list/', UserShopsListView.as_view(), name='shop-edit'),
    path('shop-edit/<int:pk>/', ShopEditView.as_view(), name='shop-edit-detail'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
