from django.shortcuts import render
from django.views.generic import View
from django.shortcuts import render
from rest_framework.generics import ListAPIView, UpdateAPIView
from .models import Shop
from .serializers import ShopSerializer, ShopSerializerUp
from .tables import ShopTables
from django_tables2 import SingleTableView
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import permissions
from django.http import HttpResponse
from django.contrib import messages
from .forms import ShopForm
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
# Create your views here.


class ShopListView(ListAPIView):
    serializer_class = ShopSerializer
    queryset = Shop.objects.all()

    def get_queryset(self):
        return Shop.objects.all()


def marker_image(request):
    image_path = 'images/marker.png'
    with open(image_path, 'rb') as f:
        return HttpResponse(f.read(), content_type='image/png')

class MapView(SingleTableView):
    model = Shop
    template_name = 'map.html'
    table_class = ShopTables

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Dodaj własny klucz do kontekstu
        context['green_icon_path'] = "images/marker.png"  # Zmień na odpowiednią ścieżkę do obrazu

        return context


def map_view(request):
    return render(request, 'map.html')


class LocationUpdateView(UpdateAPIView):
    queryset = Shop.objects.all()
    serializer_class = ShopSerializerUp
    permission_classes = [permissions.IsAuthenticated]


@api_view(['GET', 'PUT'])
def location_detail(request, pk):
    location = get_object_or_404(Shop, pk=pk)

    if request.method == 'GET':
        serializer = ShopSerializer(location)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ShopSerializer(location, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=400)

def create_shop(request):
    if request.method == 'POST':
        form = ShopForm(request.POST)
        new_shop = Shop()
        days_fields = []
        for day, day_name in ShopForm.DAYS_OF_WEEK:
            open_hour = form.data[f"{day}_open"]
            close_hour = form.data[f"{day}_close"]
            days_fields.append(f"{open_hour} - {close_hour}")
        new_shop.company = form.data['company']
        new_shop.opening_hours = days_fields
        new_shop.address_latitude = form.data['address_latitude']
        new_shop.address_longitude = form.data['address_longitude']
        new_shop.address = form.data['address']
        new_shop.type = form.data['type']
        new_shop.description = form.data['description']
        new_shop.owner = request.user
        new_shop.save()
        # print("a")
        # form = ShopForm(request.POST)
        # print("c")
        # print(form)
        # new_shop = form.save()
        messages.success(request, "Shop created successfully!")
        return redirect('map')  # Redirect to a success page or another relevant view
    else:
        print("b")
        form = ShopForm()
    
    # Prepare day-specific fields for the template
    days_fields = []
    for day, day_name in ShopForm.DAYS_OF_WEEK:
        # open_hour = form[f"{day}_open"]
        # close_hour = form[f"{day}_close"]
        # days_fields.append(f"{open_hour} - {close_hour}")
        days_fields.append({
            'day': day_name,
            'open_field': form[f"{day}_open"],
            'close_field': form[f"{day}_close"]
        })

    context = {
        'form': form,
        'days_fields': days_fields
    }

    return render(request, 'new_shop.html', context)

from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

class UserShopsListView(LoginRequiredMixin, ListView):
    model = Shop
    template_name = 'user_shops.html'
    context_object_name = 'shops'

    def get_queryset(self):
        return Shop.objects.filter(owner=self.request.user)


from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .forms import EditShopForm
from .models import Shop

class ShopEditView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Shop
    form_class = EditShopForm
    template_name = 'shop_edit.html'
    context_object_name = 'shop'

    def get_success_url(self):
        return reverse_lazy('map')

    def test_func(self):
        # Check that the shop being edited is owned by the current user
        shop = self.get_object()
        return shop.owner == self.request.user

    def form_valid(self, form):
        # Custom logic or additional processing before saving can go here
        return super().form_valid(form)
