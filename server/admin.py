# admin.py

from django.contrib import admin
from django.shortcuts import redirect

class MyAdminSite(admin.AdminSite):
    def login(self, request, extra_context=None):
        response = super().login(request, extra_context=extra_context)
        if request.method == 'POST' and request.user.is_authenticated and request.user.is_staff:
            return redirect('map')  # Redirect to 'map/' named URL after successful login
        return response

admin_site = MyAdminSite()
