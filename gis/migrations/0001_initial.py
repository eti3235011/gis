# Generated by Django 4.2.9 on 2024-01-12 22:41

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company', models.CharField(max_length=128, verbose_name='Nazwa firmy')),
                ('address', models.DecimalField(decimal_places=14, max_digits=16, verbose_name='Współrzędne adresu')),
                ('opening_hours', models.JSONField(default=dict, verbose_name='Godziny otwarcia')),
                ('type', models.CharField(max_length=128, verbose_name='Typ sklepu')),
                ('description', models.CharField(max_length=512, verbose_name='Opis')),
            ],
        ),
    ]
