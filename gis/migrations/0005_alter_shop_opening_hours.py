# Generated by Django 4.2.9 on 2024-01-13 08:26

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gis', '0004_shop_address_alter_shop_opening_hours'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shop',
            name='opening_hours',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=128), blank=True, default=['6:00 - 23:00', '6:00 - 23:00', '6:00 - 23:00', '6:00 - 23:00', '6:00 - 23:00', '6:00 - 23:00', '6:00 - 23:00'], null=True, size=None),
        ),
    ]
