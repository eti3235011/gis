from .models import Shop
from rest_framework import serializers


class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = '__all__'


class ShopSerializerUp(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ('company', 'address', 'opening_hours', 'description')
